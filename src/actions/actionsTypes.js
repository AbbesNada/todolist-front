export const LOGIN_ADMIN="LOGIN_ADMIN";
export const LOGOUT="LOGOUT";
export const FETCH_TASKS="FETCH_TASKS";
export const ADD_TASK="ADD_TASK";
export const DELETE_TASK="DELETE_TASK";
export const ERROR_STATUS="ERROR_STATUS";
export const CHANGE_TASK="CHANGE_TASK";