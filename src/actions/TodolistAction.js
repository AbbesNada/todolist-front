import axios from 'axios';
import { TIMEOUT } from '../helpers/constants.js';
import { FETCH_TASKS, ADD_TASK, DELETE_TASK, ERROR_STATUS, CHANGE_TASK } from './actionsTypes';

export const register = async(newUser) => {
   
    const response = await axios.post('http://127.0.0.1:8000/api/register',
        { name: newUser.name, password: newUser.password, email: newUser.email });
        console.log(response);
}

export const login = async(user) => {
    const response = await axios
        .post('http://127.0.0.1:8000/api/login',
            { password: user.password, email: user.email })
        .then(res => {
            localStorage.setItem('token', res.data.token)
        });
        console.log(response);
}

export const fetchTasks = () => async dispatch => {  

    let config = {
        method: 'get',
        url: `http://127.0.0.1:8000/api/tasks`,
        headers:{Authorization: `Bearer ${localStorage.getItem('token')}`},
        timeout: {TIMEOUT},
    }
    
        await axios(config).then(response =>{
            dispatch({ type: FETCH_TASKS, payload: response.data })
            console.log("tasks: "+response.data)
        });
}

export const changeTask = (term) => {
    return dispatch => (
        dispatch({ type: CHANGE_TASK, task: term })
    )
}

export const addTask = (newTask) => async dispatch => { 
    let config = {
        method: 'post',
        url: `http://127.0.0.1:8000/api/task/`,
        headers:{Authorization: `Bearer ${localStorage.getItem('token')}`},
        timeout: {TIMEOUT},
        data: newTask
    }
        await axios(config).then(response => {
            dispatch({ type: ADD_TASK, status: response.status});
        }).catch(error=> {
            dispatch({ type: ERROR_STATUS, status: error.response.status, message: error.message});
        });
}

export const deleteTask = (idtask) => {  
    let config = {
        method: 'delete',
        url: `http://127.0.0.1:8000/api/task`+idtask,
        headers:{Authorization: `Bearer ${localStorage.getItem('token')}`},
        timeout: {TIMEOUT},
    }
        axios(config);
        return dispatch => (
            dispatch({ type: DELETE_TASK, id:idtask })
        )
}