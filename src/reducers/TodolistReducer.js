import { FETCH_TASKS, ADD_TASK, DELETE_TASK, ERROR_STATUS, CHANGE_TASK } from '../actions/actionsTypes';

const INITIAL_STATE = { 
    id:'',
    user_id: '',
    description: '',
    task: '',
    tasks: []
};

export default (state = INITIAL_STATE, action) => {
    console.log("type2: "+action)
    switch (action.type) {
        case FETCH_TASKS:
            return {...state, tasks: action.payload.data }
        case ADD_TASK:
            return {...state, id: action.id, description: action.description }
        case DELETE_TASK:
            return {...state, tasks: state.tasks.filter(task => task.id !== action.id)}
        case ERROR_STATUS:
            return {...state, status: action.status }
        case CHANGE_TASK:
            return {...state, task: action.task}
        default:
            return state;
    }
}