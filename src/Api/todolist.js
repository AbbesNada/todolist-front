import axios from 'axios';

const KEY = 'token';

export default axios.create({
    baseURL:'http://127.0.0.1:8000/',
    params: {
        id: 'id',
        key: KEY
    }
});