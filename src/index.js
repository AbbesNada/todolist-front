/*import React from 'react';
import ReactDOM from 'react-dom';
import registerServiceWorker from './registerServiceWorker';
import { HashRouter } from 'react-router-dom';
import Login from './Components/Login';
import TaskBar from './Components/TaskBar';
import configureStore from './config/configureStore';
import { Provider } from 'react-redux';
import './index.css';

const store = configureStore();
const rootElement = document.getElementById('root');

const renderApp = Component => {
  ReactDOM.render(
    <Provider store={store}>
        <Component />
    </Provider>,
    rootElement
  );
};

renderApp(Login);
if(localStorage.getItem('token') == null){
  renderApp(Login);
}else{
  renderApp(TaskBar);
}

registerServiceWorker();*/

import React from 'react'
import { render } from 'react-dom'
import { Provider } from 'react-redux'
import { createStore } from 'redux'
import rootReducer from './reducers'
import App from './Components/App'
import configureStore from './config/configureStore';

const store = configureStore();

render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('root')
)