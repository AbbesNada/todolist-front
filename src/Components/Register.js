import React from 'react';
import {register} from '../actions/TodolistAction';

class Register extends React.Component {

    constructor() {
        super();
        this.state = {
            name: '',
            email: '',
            password: ''
        }

        this.onChange = this.onChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
    }

    onChange(e) {
        this.setState({ [ e.target.name ]: e.target.value })
    }

    onSubmit = async (e) => {
        e.preventDefault();
        const newUser = {
            name: this.state.name,
            email: this.state.email,
            password: this.state.password
        }

        const {history} = this.props;

        register(newUser).then(Response => {
            console.log(Response);
            history.push('/login');
        })
    }

    render() {
        return (
            <div className="container">
                <div className="row">
                    <div className="col-md-6 mt-5 mx-auto">
                        <form noValidate onSubmit={this.onSubmit}>
                            <h1 className="h3 mb-3 font-weight-normal">Register</h1>
                            <div className="form-group">
                                <label htmlFor="name">Name</label>
                                <input type="text" 
                                    className="form-control"
                                    name="name"
                                    placeholder="Nom"
                                    value={this.state.name}
                                    onChange={this.onChange} />
                            </div>
                            <div className="form-group">
                                <label htmlFor="email">Email</label>
                                <input type="email" 
                                    className="form-control"
                                    name="email"
                                    placeholder="Email"
                                    value={this.state.email}
                                    onChange={this.onChange} />
                            </div>
                            <div className="form-group">
                                <label htmlFor="password">Password</label>
                                <input type="password" 
                                    className="form-control"
                                    name="password"
                                    placeholder="Password"
                                    value={this.state.password}
                                    onChange={this.onChange} />
                            </div>
                            <button className="btn btn-lg btn-primary btn-block" >Enregistrer</button>
                        </form>
                    </div>
                </div>
            </div>
        );
    }
}

export default Register;