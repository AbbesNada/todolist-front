import React from 'react';
import { connect } from 'react-redux';
import { fetchTasks, addTask, deleteTask, changeTask } from '../actions';
import { TASK_TABLE_HEADER } from '../helpers/strings';

class TodoList extends React.Component {
    
    constructor(props) {
        super(props);
    }

    handleTaskChange = async (e)=>{
        this.props.changeTask(e.target.value);
    } 

    componentDidMount() {
        
        this.props.fetchTasks();
    }

    onSubmit = async (e) => {
        e.preventDefault();

        console.log("testtesttest");
        console.log("values:"+e.target.value);

        const newTask = {
            title: this.state.title,
            description: this.state.description,
        }

        this.props.addTask(newTask);
        console.log("task:"+newTask);
    };

    onDelete = (e) => {
        e.preventDefault();
        
        let idtask = e.target.value;

        this.props.deleteTask(idtask);
    }

    render() {
        
        let filtredTasks = this.props.tasks;
        return (
            <div>
                <header>
                    <form onSubmit = {this.onSubmit}>
                        <input type="text" placeholder="Ecrire une tâche"
                            id= "title"
                            name= "title"
                            value= {this.props.title}
                            //onChange= {this.onChange} 
                        />
                        <button type="submit" >Ajouter</button>
                    </form>
                    <table>
                        <thead>
                            <tr>
                                {TASK_TABLE_HEADER.map((item,key) => {
                                    return <th key={key}>{item}</th>
                                })}
                            </tr>
                        </thead>
                        <tbody>
                            {this.state.tasks.map(task => {
                                return (
                                <tr key={task.id}>
                                    <th scope="row">{task.id}</th>
                                    <td>{task.description}</td>
                                    <td>
                                        <button type="button" rel="tooltip" data-placement="left" title="" className="btn btn-danger btn-simple btn-icon "
                                                data-original-title="Remove Post" value={task.id} onClick={ ()=> this.onDelete.bind(this)}>
                                            <i className="fa fa-times"></i>
                                        </button>
                                    </td>
                                </tr>
                                )}
                            )}
                        </tbody>
                    </table>
                </header>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return { tasks: state.tasks.tasks,
             id: state.tasks.id }
}
 
export default connect(mapStateToProps, { fetchTasks, addTask, deleteTask, changeTask }) (TodoList);
 