import React from 'react';
import {login} from '../actions/TodolistAction';

class Login extends React.Component {

    constructor() {
        super();
        this.state = {
            email: '',
            password: '',
            errors: {}
        }

        this.onChange = this.onChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
    }

    onChange(e) {
        this.setState({ [ e.target.name ]: e.target.value })
    }

    onSubmit = async (e) => {
        e.preventDefault();
        const user = {
            email: this.state.email,
            password: this.state.password
        }
        const {history} = this.props; 

        login(user).then(Response => {
            if (Response) {
                console.log(Response);
            }
            console.log(Response);

            history.push('/task');
        })

        
    }

    render() {
        return (
            <div className="container">
                <div className="row">
                    <div className="col-md-6 mt-5 mx-auto">
                        <form noValidate onSubmit={this.onSubmit}>
                            <h1 className="h3 mb-3 font-weight-normal">Login</h1>
                            <div className="form-group">
                                <label htmlFor="email">Email</label>
                                <input type="email" 
                                    className="form-control"
                                    name="email"
                                    placeholder="Email"
                                    value={this.state.email}
                                    onChange={this.onChange} />
                            </div>
                            <div className="form-group">
                                <label htmlFor="password">Password</label>
                                <input type="password" 
                                    className="form-control"
                                    name="password"
                                    placeholder="Password"
                                    value={this.state.password}
                                    onChange={this.onChange} />
                            </div>
                            <button className="btn btn-lg btn-primary btn-block" >Se Connecter</button>
                        </form>
                    </div>
                </div>
            </div>
        );
    }
}

export default Login;