import React from 'react';
import './App.css';
import Register from './Register';
import Login from './Login';
import TodoList from './TodoList';
import { BrowserRouter as Router , Route} from 'react-router-dom';

class App extends React.Component {
  
  render() {
    return (
      <Router>
        <div className="App">
        <div className="container">
          <Route exact path="/register" component={Register} />
          <Route exact path="/" component={Login} />
          <Route exact path="/task" component={TodoList} />
        </div>
      </div>
      </Router>
    );
  }
}

export default App;
